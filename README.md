## Application setup process
### Clone the code from
git clone https://haseenkns@bitbucket.org/haseenkns/cashregister.git

### Clone database from db server.
database detail is provided in `.env` file located under app root.


### Available artisan custom commands:
- try `php artisan list` to show all the available command. Below is a list of custom command.
 - migrate
 - db:seed


- Login
  -url: http://localhost/cashRegister/public/api/login
  -parameters to pass: email,password
  -method: post
       
  -Response
  
    {
        "success": true,
        "data": {
            "success": 1,
            "user_info": {
                "user_id": 1,
                "email": "admin@gmail.com",
                "name": "Admin"
            },
            "msg": "Login Successfully",
            "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjQ2NTRmNGIyODY1Y2M5OWMzNjYzZGFiYjY0YTY0OWNhNmU2OWMzMmExNjk0ZDM1MWExZTMzNTBhOGUxYWQwY2NjNDJkYjlhN2FkZTliNjA2In0.eyJhdWQiOiIxIiwianRpIjoiNDY1NGY0YjI4NjVjYzk5YzM2NjNkYWJiNjRhNjQ5Y2E2ZTY5YzMyYTE2OTRkMzUxYTFlMzM1MGE4ZTFhZDBjY2M0MmRiOWE3YWRlOWI2MDYiLCJpYXQiOjE1NTYwNDI4MDgsIm5iZiI6MTU1NjA0MjgwOCwiZXhwIjoxNTg3NjY1MjA4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.wMBoqsftlvqQstxAERV06C70NlAoogQQkcFJn2tuCM9yjIkUgA-jbgXOuH1C4-iAlddGKJNfrkjy5lqiwDyNxJx88DBk12CxuWkt0-yt25FSVyOR2mAhWEjTBQy_W6QQSwnWPenTBp384nfSkipnRb15nbtTGkUGCrBVxIgkv5aZten5qFNAmsx7GrI8-EhWavee-KqUwWbFMAmj9BM3p0sAfl4YfYix4KChneHPH44yYL5_Kzy9YuVonVV5gbteipSVBXzn7G_HresyJ79R_QlTdSDme1e903agVBnQ8xqo8PRAdmYXtqb1po65J16-BxhX8kiNB14xka7WVAkpzNugVQH9fj-0lQbF6Dy2PjlA5nPSj868qr5qV3d2N09BMVsj2odRzqtv46fBrsp8T02UfTVx-bbhQq17GlXc1_4Daqvs0ZtGJI54vkDtukgDeuadj0tF6cI3j6RayqbRhqSbKnhn2BtX-HYuws3Wc0SapqU3j_KkRPMTrVdd0JSuQDpkYPUX-ga668wQKoUC68OZEzNf4BPVRtGCKcE39KkO9f-eqmBRdXruYWupjooOJfbKMTYF9Kx9eSakaHVMtO1uEh3Odym8DnL5viHeXuatV29sHvtt6G0k94bFjSxZ_M-VVkwTyNy7ZB_thLldqbw2FleKg9xGabgggNrneow"
        },
        "code": 200
    }
    
- Registration
  -url: http://localhost/cashRegister/public/api/register
  -parameters to pass: email,password,name
  -method: post
       
  -Response
  
{
    "success": true,
    "data": {
        "success": 1,
        "user_info": {
            "user_id": 3,
            "name": "haseen",
            "email": "haseenkns1@gmail.com"
        },
        "msg": "Registered Successfully",
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImJiYzlmYjUyZTU0MDI3NDNiMDQ4M2UzZWU5MjU1NjE1NGI2MjBjODQ3ZmQzMmM0YTU2NTNjYTM2MjNmN2Y3MWZlMTEyYzcwM2FlNDFiODBlIn0.eyJhdWQiOiIxIiwianRpIjoiYmJjOWZiNTJlNTQwMjc0M2IwNDgzZTNlZTkyNTU2MTU0YjYyMGM4NDdmZDMyYzRhNTY1M2NhMzYyM2Y3ZjcxZmUxMTJjNzAzYWU0MWI4MGUiLCJpYXQiOjE1NTYxMDg2NjMsIm5iZiI6MTU1NjEwODY2MywiZXhwIjoxNTg3NzMxMDYzLCJzdWIiOiIzIiwic2NvcGVzIjpbXX0.WQSW9qZfsAomhon1_q7ywhaqgK7SVudNLUkevwRpBHz9JXVtpqgIA9SZcKY_JQibLqTtaIsNZPcBXV1MUOea-4z0IaJ8rwNJ91IPIkSe0pN3bsCdze50TFelOD6zEEbVB3ltTOkmm8J-bDgLcAdR6NJ0MbeKCeCFPsSpK0iy1fMHKlp_Hm5I51SKEJDdBMtWK-q1ehIHdNtRFidJce9hS-XrD4-NXwmq6F9aFUyqU-FuJekneR125apeZ1ZWqBRst7J1rvf0r3x_9RZCX7TRze2iwBoERT3z7S4nMxmTT_V_LbkFYcpPkeYpCvGqM1d25JpNiN5vw99rSnilCX0qDPxi8PFhcYZQttX--Q4t9DwAGmqgd6tLYgb9Luwb3XvutrbmYHNgZmi-_RtgbbGi64oJCF3YlF130n1IBwQvuM8v8cj3SHWEiD4rbxWJizjKjsu4JhudJoprdlnCJcCWJ53vfnNuRtNnAA6D45Doty_GeZRS6LdjiBcbKoaSw2L6BluHrFgDfCiMd2qgwnkh7lq3JhQZ-4564azeGEhpjuprwH-i74eaD-9jbPehRmNNop1r8XCnaa7L9rSMmbaK7YU3znCfVbHQddqsP8x1ZSiwV5EqTq1NcImZGQIhvjEhC_tpPP8sI9p3-1OoBsnbH2tidI0KAdI65pauh-zUNlM"
    },
    "code": 200
}
    

- Admin: Add a product (properties: barcode, name, cost, vat-class (6% or 21%))
  -url:  http://localhost/cashRegister/public/api/admin/create-product
  -parameter to pass: barcode, name, cost, vat_class
  -method: post
           
   -Response
     
   {
       "success": true,
       "msg": "Product added successfully",
       "code": 200
   } 
         
- Admin: List all products
  -url  http://localhost/cashRegister/public/api/admin/list-products
  -parameter to pass: page_number
    -method: get
           
   -Response
   
   {
       "success": true,
       "data": {
           "current_page": 1,
           "data": [
               {
                   "id": 1,
                   "barcode": "asdasda",
                   "name": "shirt",
                   "cost": 100,
                   "vat_class": 6,
                   "created_at": "2019-04-23 18:56:32",
                   "updated_at": "2019-04-23 18:56:32"
               }
           ],
           "first_page_url": "http://localhost/cashRegisteration/public/api/admin/list-products?=1",
           "from": 1,
           "last_page": 1,
           "last_page_url": "http://localhost/cashRegisteration/public/api/admin/list-products?=1",
           "next_page_url": null,
           "path": "http://localhost/cashRegisteration/public/api/admin/list-products",
           "per_page": 10,
           "prev_page_url": null,
           "to": 1,
           "total": 1
       },
       "code": 200
   }
   
- Cash register: Create a new receipt
  -url:  http://localhost/cashRegister/public/api/admin/create-receipt
  -parameter to pass: 
  -method: post
  
  -Response
  
  {
      "success": true,
      "data": {
          "receipt_number": 7936,
          "receipt_id": 1
      },
      "code": 200
  }
  
- Cash register: Add a product by barcode to the receipt
  -url:  http://localhost/cashRegister/public/api/admin/add-product
  -parameter to pass: barcode, receipt_id
  -method: post 
    
      -Response
      
      {
          "success": true,
          "data": {
              "receipt_number": 7936,
              "id": 1
          },
          "code": 200
      }
      
- Cash register: Finish a receipt
        -url:  http://localhost/cashRegister/public/api/admin/finish-receipt
        -parameter to pass: receipt_id
        -method: post 
          
            -Response
            
           {
                  "success": true,
                  "msg": "Receipt is finished",
                  "code": 200
              } 