<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');

Route::group(['middleware' => 'auth:api'], function() {

    Route::post('create-receipt', 'API\UserController@createReceipt');
    Route::post('add-product', 'API\UserController@addProduct');
    Route::post('finish-receipt', 'API\UserController@finishReceipt');

    Route::group(['prefix' => 'admin'], function () {
        Route::post('/create-product', 'API\UserController@createProduct')->name('create-product');
        Route::get('/list-products', 'API\UserController@listsProducts')->name('list-products');
    });
});
