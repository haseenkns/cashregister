<?php

namespace App\Http\Controllers\API;

use App\Receipt;
use App\ReceiptProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\OauthAccessToken;
use Validator;
use App\Product;
use App\User;

/*
 * 200: OK. The standard success code and default option.
201: Object created. Useful for the store actions.
204: No content. When an action was executed successfully, but there is no content to return.
206: Partial content. Useful when you have to return a paginated list of resources.
400: Bad request. The standard option for requests that fail to pass validation.
401: Unauthorized. The user needs to be authenticated.
403: Forbidden. The user is authenticated, but does not have the permissions to perform an action.
404: Not found. This will be returned automatically by Laravel when the resource is not found.
500: Internal server error. Ideally you're not going to be explicitly returning this, but if something unexpected breaks, this is what your user is going to receive.
503: Service unavailable. Pretty self explanatory, but also another code that is not going to be returned explicitly by the application.
*/

class UserController extends Controller
{
    //
    public $successStatus = 200;

    public function login(Request $request)
    {
        $rules = [
            'email' => 'required|email',
            'password' => 'required',
        ];

        $validator = Validator::make(request()->all(), $rules);
        if ($validator->fails()) {
            $messages = $validator->messages();
            foreach ($messages->all() as $m) {
                $msg[] = $m;
            }
            return response()->json(['success' => false, 'errorCode' => 0, 'errors' => $msg, 'code' => 401]);
        } else {
            if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
                $user = Auth::user();
                $success['token'] = $user->createToken('MyApp')->accessToken;
                $success['user_id'] = $user->id;
                $user_info = array(
                    'user_id' => $user->id,
                    'email' => $user->email,
                    'name' => ucfirst($user->name),
                );
//                User::where('id',$user->id)->update(['device_token'=>request('device_token'),'device_type'=>request('device_type')]);
                $data = array(
                    'success' => 1, 'user_info' => $user_info, 'msg' => 'Login Successfully', 'token' => $success['token']
                );
                return response()->json(['success' => true, 'data' => $data, 'code' => $this->successStatus]);

            } else {
                $msg[] = 'Invalid email address or password provided';
                return response()->json(['success' => false, 'errorCode' => 0, 'errors' => $msg, 'code' => 401]);
            }
        }
    }


    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
        ];

        $validator = Validator::make(request()->all(), $rules);
        if ($validator->fails()) {
            $messages = $validator->messages();
            foreach ($messages->all() as $m) {
                $msg[] = $m;
            }
            return response()->json(['success' => false, 'errorCode' => 0, 'errors' => $msg, 'code' => 401]);
        } else {
            $user = new User;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->save();

            $token = $user->createToken('MyApp')->accessToken;

            $user_info = array(
                'user_id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
            );
            $data = array(
                'success' => 1, 'user_info' => $user_info, 'msg' => 'Registered Successfully', 'token' => $token
            );
            return response()->json(['success' => true, 'data' => $data, 'code' => $this->successStatus]);
        }

    }

    public function listsProducts(Request $request){
        $rules = [
            'page_number' => 'required',
        ];
        $validator = Validator::make(request()->all(), $rules);
        if ($validator->fails()) {
            $messages = $validator->messages();
            foreach ($messages->all() as $m) {
                $msg[] = $m;
            }
            return response()->json(['success' => false, 'errorCode' => 0, 'errors' => $msg, 'code' => 401]);
        } else {
            $products=Product::orderBy('id','desc')->paginate(10, ['*'], '', $request->page_number);
            return response()->json(['success' => true, 'data' => $products, 'code' => $this->successStatus]);
        }
    }


    public function createProduct(Request $request){

        $rules = [
            'barcode' => 'required|unique:products',
            'name' => 'required',
            'cost' => 'required',
            'vat_class' => 'required',
        ];
        $validator = Validator::make(request()->all(), $rules);
        if ($validator->fails()) {
            $messages = $validator->messages();
            foreach ($messages->all() as $m) {
                $msg[] = $m;
            }
            return response()->json(['success' => false, 'errorCode' => 0, 'errors' => $msg, 'code' => 401]);
        } else {
            $storeCard = new Product;
            $storeCard->barcode = $request->barcode;
            $storeCard->name = $request->name;
            $storeCard->cost = $request->cost;
            $storeCard->vat_class = $request->vat_class;
            $storeCard->save();

            if($storeCard){
                return response()->json(['success' => true, 'msg' => 'Product added successfully', 'code' => $this->successStatus]);
            }else{
                return response()->json(['success' => false, 'msg' => 'Something went wrong', 'code' => 401]);
            }
        }
    }

    public function createReceipt(Request $request){

            $lastReceiptId=Receipt::orderBy('id','desc')->first();
            $receiptCount=(isset($lastReceiptId->id)?$lastReceiptId->id:0)+1000;

            $receipt=new Receipt;
            $receipt->user_id=\Auth::user()->id;
            $receipt->receipt_number=$receiptCount+rand(1111,9999);
            $receipt->status=0;//Receipt Incomplete
            $receipt->save();

            if($receipt){
                $data=array(
                    'receipt_number'=>$receipt->receipt_number,
                    'receipt_id'=>$receipt->id,
                );
                return response()->json(['success' => true, 'data' => $data, 'code' => $this->successStatus]);
            }else{
                return response()->json(['success' => false, 'msg' => 'Something went wrong', 'code' => 401]);
            }
    }


    public function addProduct(Request $request){
        $rules = [
            'barcode' => 'required',
            'receipt_id' => 'required',
        ];
        $validator = Validator::make(request()->all(), $rules);
        if ($validator->fails()) {
            $messages = $validator->messages();
            foreach ($messages->all() as $m) {
                $msg[] = $m;
            }
            return response()->json(['success' => false, 'errorCode' => 0, 'errors' => $msg, 'code' => 401]);
        } else {
            $receiptStatus=Receipt::where('id',$request->receipt_id)->first();
            if($receiptStatus->status==0) {
                $product = new ReceiptProduct;
                $product->barcode = $request->barcode;
                $product->user_id = \Auth::user()->id;
                $product->receipt_id = $request->receipt_id;
                $product->save();

                if ($product) {
                    $data = array(
                        'receipt_number' => $product->receipt_id,
                        'id' => $product->id
                    );
                    return response()->json(['success' => true, 'data' => $data, 'code' => $this->successStatus]);
                } else {
                    return response()->json(['success' => false, 'msg' => 'Something went wrong', 'code' => 401]);
                }
            }else{
                return response()->json(['success' => false, 'msg' => 'Receipt is finished, you cannot add products any more to this receipt', 'code' => 401]);
            }
        }
    }

    public function finishReceipt(Request $request){
        $rules = [
            'receipt_id' => 'required',
        ];
        $validator = Validator::make(request()->all(), $rules);
        if ($validator->fails()) {
            $messages = $validator->messages();
            foreach ($messages->all() as $m) {
                $msg[] = $m;
            }
            return response()->json(['success' => false, 'errorCode' => 0, 'errors' => $msg, 'code' => 401]);
        } else {
            Receipt::where('id',$request->receipt_id)->update(['status'=>1]);
            return response()->json(['success' => true, 'msg' => 'Receipt is finished', 'code' => $this->successStatus]);
        }
    }


}
